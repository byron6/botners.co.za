<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Text Plugin Admin View
 *
 * @package    YITH
 * @author     Armando Liccardo <armando.liccardo@yithemes.it>
 * @since      2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly.

extract( $field );

$currency = get_woocommerce_currency();
?>

<input type="text" name="<?php echo esc_html( $name )?>" id="<?php echo esc_html( $id )?>" value="<?php echo ( !empty( $value) ) ? esc_html( $value ) : '' ?>" class="<?php echo ( !empty( $class) ) ? esc_html( $class ) : '' ?>">
<?php if( $id !== 'ywpar_max_points_product_discount') : ?>
<span class="<?php echo esc_html( $name )?>_currency"><?php echo get_woocommerce_currency_symbol( $currency ) . ' (' . $currency . ')'; ?></span>
<?php endif; ?>