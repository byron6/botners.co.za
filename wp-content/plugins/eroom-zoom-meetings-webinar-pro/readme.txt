=== eRoom - Zoom Meetings & Webinar Pro ===

Contributors: StylemixThemes
Donate link: https://stylemixthemes.com/
Tags: zoom, meetings, webinar
Requires at least: 4.6
Tested up to: 5.4
Stable tag: 1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Changelog ==

= 1.0.3 =
* Zoom Webinars added for Zoom Conference Product.
* WooCommerce Virtual Product option added for Zoom Conference.
* WooCommerce Downloadable Product option added for Zoom Conference.
* WooCommerce Product displaying Conference Date issue fixed.
* WooCommerce Product page "Webinar starts in" duplicated text removed.
* Zoom Pro menu name changed into eRoom Pro.
* STM_ZOOM_PRO Class name changed into StmZoomPro.

= 1.0.2 =
* Minor bug fixes.

= 1.0.1 =
* Meetings Grid shortcode added.
* Single Meeting page added.

= 1.0.0 =
* First Release.