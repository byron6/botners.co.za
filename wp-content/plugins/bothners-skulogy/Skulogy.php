<?php
/*
Plugin Name: Skulogy
Plugin URI: https://www.semantica.co.za
Description: Skulogy Integration.
Version: 1.0
Author: Semantica Digital
Author URI: https://www.semantica.co.za
License: A "Slug" license name e.g. GPL2
*/
require 'vendor/autoload.php';

use GuzzleHttp\Client;


class Skulogy
{
    private $options;
    const SKULOGY_API_ENDPOINT = 'https://skulogy.com/api/v2/';
    //https://skulogy.com/api/v2/APIKEY/setprice?p=ref123&amt=99.90&cost=75.00&cur=usd
    const WC_REST_KEY = 'ck_9322158f76484d2c94d8160d11e195fce01d4bba';
    const WC_REST_SECRET = 'cs_a63923c6ddfc2ae5764bd75d3482c425af89673b';

    public function __construct()
    {
        //add_action('admin_enqueue_scripts', array($this, 'load_custom_scripts'));
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
        add_action('woocommerce_init', array($this, 'update_skulogy_prices_json'));
        add_action('woocommerce_init', array($this, 'test_skulogy_prices'));
        add_action('woocommerce_init', array($this, 'set_skulogy_cog'));
    }

    public function add_plugin_page()
    {
        add_submenu_page('woocommerce', 'Skulogy', 'Skulogy Settings', 'manage_options', 'skulogy-admin', array($this, 'create_admin_page'));
        //add_submenu_page( 'woocommerce', 'Skulogy', 'Skulogy Report', 'manage_options', 'skulogy-reports', array( $this, 'create_report_page' ));
    }


    public function create_report_page()
    {
        ?>
        <div class="jumbotron">
            <div id="mount"></div>
        </div>
        <?php
    }

    public function create_admin_page()
    {
        $this->options = get_option('skulogy');
        ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8"
                src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script>
            jQuery(document).ready(function () {
                jQuery('#skulogy_sync_found').DataTable();
                jQuery('#skulogy_sync_nfound').DataTable();
                jQuery('#skulogy_sync_cog_found').DataTable();
                jQuery('#skulogy_sync_cog_nfound').DataTable();
            });

        </script>
        <div class="wrap">
            <form method="post" action="options.php">
                <?php
                settings_fields('my_option_group');
                do_settings_sections('skulogy-admin');
                submit_button();
                ?>
            </form>
        </div>
        <?php
        $this->show_cron_history();
        $this->show_cog_history();
    }

    public function show_cog_history() {
        $cron_data = unserialize(get_transient('skulogy_cog_update'));
        ?>
        <div class="jumbotron">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <h4 class="page-title">Skulogy Sync History - Cost of Goods</h4>
                    <em>Last Update: <?php echo $cron_data['last_run'] ?> </em>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <div class="row">
                            <div style="background: white; padding: 30px;">
                                <table id="skulogy_sync_cog_found" class="display compact" style="width:100%">
                                    <caption>
                                        Skulogy: <i>Matching SKUs - Cost Of Goods</i>
                                    </caption>
                                    <thead>
                                    <tr>
                                        <th scope="col">SKU</th>
                                        <th scope="col">Message</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach (array_keys($cron_data['found']) as $sku): ?>
                                        <tr>
                                            <th scope="row"><?php echo $sku; ?></th>
                                            <td><?php echo $cron_data['found'][$sku]; ?></td>
                                        </tr>

                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="card-box">
                        <div style="margin: -15px;margin-top: 10px;">
                            <div style="background: white; padding: 30px;">
                                <table id="skulogy_sync_cog_nfound" class="display compact" style="width:100%">
                                    <caption>
                                        Skulogy: <i>Non-Matching SKUs - Cost Of Goods</i>
                                    </caption>
                                    <thead>
                                    <tr>
                                        <th scope="col">SKU</th>
                                        <th scope="col">Message</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach (array_keys($cron_data['not_found']) as $sku): ?>
                                        <tr>
                                            <th scope="row"><?php echo $sku; ?></th>
                                            <td><?php echo $cron_data['not_found'][$sku]; ?></td>
                                        </tr>

                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php

    }

    public function show_cron_history()
    {
        $sync_url = get_home_url() . '/?update_skulogy_prices&key=' . get_option('skulogy')['bothner_key'] . '&method=admin';
        $cron_data = unserialize(get_transient('skulogy_cron'));
//
        ?>
        <div class="jumbotron">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <h4 class="page-title">Skulogy Sync History - Reprice</h4><a href="<?php echo $sync_url; ?>" target="_blank"
                                                                       class="btn btn-primary">Run Sync</a>
                    <em>Last Update: <?php echo $cron_data['last_run'] ?> (<?php echo $cron_data['method'] ?>)</em>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <div class="row">
                            <div class="card">
                                <table id="skulogy_sync_found" class="display compact" style="width:100%">
                                    <caption>
                                        Skulogy: <i>Matching SKUs</i>
                                    </caption>
                                    <thead>
                                    <tr>
                                        <th scope="col">SKU</th>
                                        <th scope="col">Regular Price</th>
                                        <th scope="col">Sale Price</th>
                                        <th scope="col">Reprice</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach (array_keys($cron_data['found']) as $sku): ?>
                                        <tr>
                                            <th scope="row"><?php echo $sku; ?></th>
                                            <td><?php echo $cron_data['found'][$sku]['regular']; ?></td>
                                            <td><?php echo $cron_data['found'][$sku]['sale']; ?></td>
                                            <td><?php echo $cron_data['found'][$sku]['reprice']; ?></td>
                                        </tr>

                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-box">
                        <div class="row">
                            <div class="card">
                                <table id="skulogy_sync_nfound" class="display compact" style="width:100%">
                                    <caption>
                                        Skulogy: <i>Non-Matching SKUs</i>
                                    </caption>
                                    <thead>
                                    <tr>
                                        <th scope="col">SKU</th>
                                        <th scope="col">Regular Price</th>
                                        <th scope="col">Sale Price</th>
                                        <th scope="col">Reprice</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach (array_keys($cron_data['not_found']) as $sku): ?>
                                        <tr>
                                            <th scope="row"><?php echo $sku; ?></th>
                                            <td><?php echo $cron_data['not_found'][$sku]['regular']; ?></td>
                                            <td><?php echo $cron_data['not_found'][$sku]['sale']; ?></td>
                                            <td><?php echo $cron_data['not_found'][$sku]['reprice']; ?></td>
                                        </tr>

                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <?php
    }

    public function page_init()
    {
        register_setting(
            'my_option_group', // Option group
            'skulogy', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Skulogy Settings', // Title
            array($this, 'print_section_info'), // Callback
            'skulogy-admin' // Page
        );

        add_settings_field(
            'api_key', // ID
            'Skulogy API Key', // Title
            array($this, 'api_key_callback'), // Callback
            'skulogy-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'api_proxy_key', // ID
            'Skulogy Proxy API Key', // Title
            array($this, 'api_proxy_key_callback'), // Callback
            'skulogy-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'api_proxy_uri', // ID
            'Skulogy Proxy URI', // Title
            array($this, 'api_proxy_uri_callback'), // Callback
            'skulogy-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'api_proxy_enabled', // ID
            'Enable Skulogy Proxy', // Title
            array($this, 'api_proxy_enabled_callback'), // Callback
            'skulogy-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'bothner_key', // ID
            'Bothner Update Key', // Title
            array($this, 'bothner_key_callback'), // Callback
            'skulogy-admin', // Page
            'setting_section_id' // Section
        );

    }

    public function sanitize($input)
    {
        $new_input = array();

        if (isset($input['api_key']))
            $new_input['api_key'] = sanitize_text_field($input['api_key']);
        if (isset($input['bothner_key']))
            $new_input['bothner_key'] = sanitize_text_field($input['bothner_key']);
        if (isset($input['api_proxy_key']))
            $new_input['api_proxy_key'] = sanitize_text_field($input['api_proxy_key']);
        if (isset($input['api_proxy_uri']))
            $new_input['api_proxy_uri'] = sanitize_text_field($input['api_proxy_uri']);
        if (isset($input['api_proxy_enabled']))
            $new_input['api_proxy_enabled'] = sanitize_text_field($input['api_proxy_enabled']);




        return $new_input;
    }

    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    public function api_key_callback()
    {
        printf(
            '<input type="text" id="api_key" name="skulogy[api_key]" value="%s" />',
            isset($this->options['api_key']) ? esc_attr($this->options['api_key']) : ''
        );
    }

    public function bothner_key_callback()
    {
        printf(
            '<input type="text" id="api_key" name="skulogy[bothner_key]" value="%s" />',
            isset($this->options['bothner_key']) ? esc_attr($this->options['bothner_key']) : ''
        );
    }

    public function api_proxy_key_callback()
    {
        printf(
            '<input type="text" id="api_key" name="skulogy[api_proxy_key]" value="%s" />',
            isset($this->options['api_proxy_key']) ? esc_attr($this->options['api_proxy_key']) : ''
        );
    }

    public function api_proxy_uri_callback()
    {
        printf(
            '<input type="text" id="api_key" name="skulogy[api_proxy_uri]" value="%s" />',
            isset($this->options['api_proxy_uri']) ? esc_attr($this->options['api_proxy_uri']) : ''
        );
    }

    function api_proxy_enabled_callback() {

        $html = '<input type="checkbox" id="api_key" name="skulogy[api_proxy_enabled]" value="1"' . checked( 1, $this->options['api_proxy_enabled'], false ) . '/>';
//        $html .= '<label for="skulogy[api_proxy_enabled]">Enable API Proxy</label>';

        echo $html;

    }

    static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    static function str_lreplace($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);

        if ($pos !== false) {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }

    public function getProducts()
    {
        $use_proxy = get_option('skulogy')['api_proxy_enabled'];

        if ($use_proxy) {
            $proxy_key = get_option('skulogy')['api_proxy_key'];
            $proxy_uri = get_option('skulogy')['api_proxy_uri'];
            $params = base64_encode(serialize(json_encode(['key' => $proxy_key])));
            $uri = $proxy_uri.'skulogy/'.$params;
        } else {
            $skulogy_key = get_option('skulogy')['api_key'];
            $uri = self::SKULOGY_API_ENDPOINT . $skulogy_key . '/getproducts.json?cur=ZAR';
        }

        $client = new GuzzleHttp\Client();
//        $res = $client->request('GET', self::SKULOGY_API_ENDPOINT . $skulogy_key . '/getproducts.json?cur=ZAR',
//            [
//                // 'headers' => $headers
//            ]);
        $res = $client->request('GET', $uri,
            [
                // 'headers' => $headers
            ]);


        $products = json_decode($res->getBody()->getContents(), true);
        $comp_products = [];

        $competitors = [];
        $competitors_full_keys = [];

        foreach (array_keys($products) as $sku) {
            if ($sku != "productscount" && $sku != "mycur") {
                if (array_key_exists('competitors', $products[$sku]) && !empty($products[$sku]['competitors'])) {
                    $c_keys = array_keys($products[$sku]['competitors']);
                    foreach ($c_keys as $c_key) {
                        $c_key_full = $c_key;
                        $c_key = explode("/", $c_key);
                        if (!in_array($c_key[0], $competitors)) {
                            $competitors[] = $c_key[0];
                        }

                        if (!array_key_exists($c_key[0], $competitors_full_keys)) {
                            $competitors_full_keys[$c_key[0]]['full'][] = $c_key_full;
                        } else {
                            if (!in_array($c_key_full, $competitors_full_keys[$c_key[0]]['full'])) {
                                $competitors_full_keys[$c_key[0]]['full'][] = $c_key_full;
                            }
                        }

                    }
                    $product = $products[$sku];
                    $product['sku'] = $sku;
                    $comp_products[] = $product;
                }
            }
        }
        return $comp_products;

    }

    private function setPrice($product, $sku, $reprice)
    {
        $product_regular_price = $product->get_regular_price();
        $product_sale_price = $product->get_sale_price();
        $product->set_sale_price($reprice);
        $product->save();
        return "<pre>SKU: " . $sku . " REG PRICE: " . doubleval($product_regular_price) . " SALE PRICE: " . $product_sale_price . " REPRICE: " . doubleval($reprice) . "</pre>";
    }

    private function setPriceJSON($product, $sku, $reprice)
    {
        $product_regular_price = $product->get_regular_price();
        $product_sale_price = $product->get_sale_price();
        $product->set_sale_price($reprice);
        $product->save();
        $data = [];
        $data['regular'] = doubleval($product_regular_price);
        $data['sale'] = $product_sale_price;
        $data['reprice'] = doubleval($reprice);
        //return "<pre>SKU: ".$sku." REG PRICE: ".doubleval($product_regular_price)." SALE PRICE: ".$product_sale_price." REPRICE: ".doubleval($reprice)."</pre>";
        return $data;
    }


    public function getNewSkuDashDecrement($dash_counter, $sku_fragments)
    {
        $k = $dash_counter - 1;
        $new_sku = "";
        for ($i = 0; $i < count($sku_fragments); $i++) {

            $new_sku .= $sku_fragments[$i];
            if ($k != 0) {
                $new_sku .= "-";
                $k--;
            } else {
                $new_sku .= " ";
            }

        }
        return trim($new_sku);
    }

    public function getNewSkuDashByOne($position, $sku_fragments)
    {
        $new_sku = "";
        for ($i = 0; $i < count($sku_fragments); $i++) {
            if ($position == $i) {
                $new_sku .= "-" . $sku_fragments[$i];
            } else {
                $new_sku .= " " . $sku_fragments[$i];
            }
        }
        return $new_sku;
    }

    public function test_skulogy_prices()
    {
        if (isset($_GET['test_skulogy_prices']) && isset($_GET['key'])) {
            $products = $this->getProducts();
            wp_send_json($products);
        }
    }

    public function set_skulogy_cog()
    {
        if (isset($_GET['semantica'])) {
            $wc_skly = $this->get_wc_skly_skus();
            $skulogy_key = get_option('skulogy')['api_key'];
            $proxy_key = get_option('skulogy')['api_proxy_key'];
            $proxy_uri = get_option('skulogy')['api_proxy_uri'];
            $use_proxy = get_option('skulogy')['api_proxy_enabled'];

            $client = new GuzzleHttp\Client();

            $responses = [];
            $res = [];
            $products = get_posts([
                'post_type' => 'product',
                'post_status' => 'publish',
                'posts_per_page' => -1
            ]);

            if (!empty($products)) {
                foreach ($products as $p) {
                    $id = $p->ID;
                    $sku = get_post_meta($id, '_sku')[0];
                    $cog = get_post_meta($id, '_wc_cog_cost')[0];
                    if (!empty($cog)) {
                        $res[] = ['id' => $id, 'sku' => $sku, 'cog' => $cog];
                    }

                }
            }
            $found = [];
            $not_found = [];
            foreach ($res as $r) {
                if (array_key_exists($r['sku'], $wc_skly)) {
                    $skulogy_response = '';
                    $skly_sku = $wc_skly[$r['sku']];
                    try {
                        $uri = self::SKULOGY_API_ENDPOINT . $skulogy_key . '/setprice?p=' . $skly_sku . '&cost=' . $r['cog'] . '&cur=zar';
                        if ($use_proxy) {
                            $params = base64_encode(serialize(json_encode(['key' => $proxy_key, 'sku' => $skly_sku, 'cost' => $r['cog']])));
                            $uri = $uri = $proxy_uri.'skulogy/'.$params;
                        } else {
                            $uri = self::SKULOGY_API_ENDPOINT . $skulogy_key . '/setprice?p=' . $skly_sku . '&cost=' . $r['cog'] . '&cur=zar';
                        }
                        $res = $client->request('GET', $uri);
                    } catch (\GuzzleHttp\Exception\ClientException $e) {
                        $skulogy_response = $e->getMessage();
                        $not_found[$skly_sku] = $skulogy_response;
                    }
                    if (!$skulogy_response) {
                        if ($use_proxy) {
                            $skulogy_response = json_decode($res->getBody()->getContents(), 'ARRAY_A');
                            if (array_key_exists('found', $skulogy_response)) {
                                $found[$skulogy_response['found']['sku']] = $skulogy_response['found']['response'];
                            }

                            if (array_key_exists('not_found', $skulogy_response)) {
                                $not_found[$skulogy_response['not_found']['sku']] = $skulogy_response['not_found']['response'];
                            }
                        } else {
                            $skulogy_response = $res->getBody()->getContents();
                            $found[$skly_sku] = $skulogy_response;
                        }
                    }
                }

            }
            set_transient('skulogy_cog_update', serialize(['found' => $found, 'not_found' => $not_found, 'last_run' => (new DateTime())->format('Y-m-d H:i:s')]), 86400);
            wp_send_json(['not_found' => $not_found, 'found' => $found]);
        }
    }

    public function get_wc_skly_skus() {
        $wc_skly_sku = [];
        $products = $this->getProducts();
        foreach ($products as $product) {
            $sku = trim($product['sku']);
            $skly_sku = $sku;
            if (Skulogy::endsWith($sku, "W-CASE")) {
                $sku = addslashes(Skulogy::str_lreplace("W-CASE", "W/CASE", $sku));
            }

            $product = wc_get_product(wc_get_product_id_by_sku($sku));
            if ($product) {
                $wc_skly_sku[$skly_sku] = $sku;
            } else {
                // Check for '+'
                $new_sku = $sku . "+";
                $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                if ($product) {
                    $wc_skly_sku[$skly_sku] = $new_sku;
                } else {
                    $sku_fragments = explode("-", $sku);
                    $dash_counter = count($sku_fragments) - 1;
                    $found_sku = false;
                    while ($dash_counter !== 0) {
                        $new_sku = trim($this->getNewSkuDashDecrement($dash_counter, $sku_fragments));
                        $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                        if ($product) {
                            $wc_skly_sku[$skly_sku] = $new_sku;
                            $found_sku = true;
                            break;
                        }

                        $dash_counter--;
                    }

                    if (!$found_sku) {
                        $dash_counter = count($sku_fragments);
                        while ($dash_counter !== 0) {
                            $new_sku = trim($this->getNewSkuDashByOne($dash_counter, $sku_fragments));
                            $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                            if ($product) {
                                $wc_skly_sku[$skly_sku] = $new_sku;
                                $found_sku = true;
                                break;
                            }

                            $dash_counter--;
                        }
                    }

                }
            }
        }
        //set_transient('wc_skly_sku', serialize(['wc_skly_sku' => $wc_skly_sku, 'last_run' => (new DateTime())->format('Y-m-d H:i:s')]), 86400);
        return array_flip($wc_skly_sku);
    }

    public function update_skulogy_prices_json()
    {
        if (isset($_GET['update_skulogy_prices']) && isset($_GET['key'])) {
            if ($_GET['key'] == get_option('skulogy')['bothner_key']) {


                $products = $this->getProducts();
                $found = [];
                $not_found = [];
                foreach ($products as $product) {
                    $sku = trim($product['sku']);

                    if (Skulogy::endsWith($sku, "W-CASE")) {
                        $sku = addslashes(Skulogy::str_lreplace("W-CASE", "W/CASE", $sku));
                    }
                    if (array_key_exists('reprice', $product)) {

                        $reprice = $product['reprice'];
                        $product = wc_get_product(wc_get_product_id_by_sku($sku));
                        if ($product) {
                            $found[$sku] = $this->setPriceJSON($product, $sku, $reprice);
                        } else {
                            // Check for '+'
                            $new_sku = $sku . "+";
                            $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                            if ($product) {
                                $found[$new_sku] = $this->setPriceJSON($product, $new_sku, $reprice);
                            } else {
                                $sku_fragments = explode("-", $sku);
                                $dash_counter = count($sku_fragments) - 1;
                                $found_sku = false;
                                while ($dash_counter !== 0) {
                                    $new_sku = trim($this->getNewSkuDashDecrement($dash_counter, $sku_fragments));
                                    $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                                    if ($product) {
                                        $found[$new_sku] = $this->setPriceJSON($product, $new_sku, $reprice);
                                        $found_sku = true;
                                        break;
                                    }

                                    $dash_counter--;
                                }

                                if (!$found_sku) {
                                    $dash_counter = count($sku_fragments);
                                    while ($dash_counter !== 0) {
                                        $new_sku = trim($this->getNewSkuDashByOne($dash_counter, $sku_fragments));
                                        $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                                        if ($product) {
                                            $found[$new_sku] = $this->setPriceJSON($product, $new_sku, $reprice);
                                            $found_sku = true;
                                            break;
                                        }

                                        $dash_counter--;
                                    }
                                }

                                if (!$found_sku) {
                                    $not_found[$sku]['reprice'] = doubleval($reprice);
                                }
                            }
                        }

                    }
                }
                $method = (isset($_GET['method'])) ? $_GET['method'] : "cron";
                //set_transient('skulogy_wc_sku', serialize($wc_skly_sku), 86400);
                set_transient('skulogy_cron', serialize(['found' => $found, 'not_found' => $not_found, 'method' => $method, 'last_run' => (new DateTime())->format('Y-m-d H:i:s')]), 86400);
//                $cog = $this->set_skulogy_cog();
                wp_send_json(['found' => $found, 'not_found' => $not_found]);

            }
        }

    }

    public function update_skulogy_prices()
    {
        if (isset($_GET['update_skulogy_prices']) && isset($_GET['key'])) {
            if ($_GET['key'] == get_option('skulogy')['bothner_key']) {

                $products = $this->getProducts();
                $found = [];
                $found[] = "<pre>============================================================</pre>";
                $found[] = "<pre> PRODUCTS FOUND</pre>";
                $found[] = "<pre>============================================================</pre>";
                $not_found = [];
                $not_found[] = "<pre>============================================================</pre>";
                $not_found[] = "<pre> SKULOGY PRODUCTS NOT FOUND</pre>";
                $not_found[] = "<pre>============================================================</pre>";
                foreach ($products as $product) {
                    $sku = trim($product['sku']);
                    //echo "<pre>INCOMING >>> : ".$sku."</pre>";
                    if (Skulogy::endsWith($sku, "W-CASE")) {
                        $sku = addslashes(Skulogy::str_lreplace("W-CASE", "W/CASE", $sku));
//                        echo "<pre>";
//                        var_dump("WITH CASE!: ".$sku);
//                        echo "</pre>";
                        // die();
                    }
                    if (array_key_exists('reprice', $product)) {
                        $reprice = $product['reprice'];
                        $product = wc_get_product(wc_get_product_id_by_sku($sku));
                        if ($product) {
                            $found[] = $this->setPrice($product, $sku, $reprice);
                        } else {
                            // Check for '+'
                            $new_sku = $sku . "+";
                            $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                            if ($product) {
                                $found[] = $this->setPrice($product, $new_sku, $reprice);
                            } else {
//                                echo "<pre>";
//                                var_dump("SCENARIO 1");
//                                echo "</pre>";
                                $sku_fragments = explode("-", $sku);
                                $dash_counter = count($sku_fragments) - 1;
                                //$dash_counter = count($sku_fragments);
                                $found_sku = false;
                                while ($dash_counter !== 0) {
                                    $new_sku = trim($this->getNewSkuDashDecrement($dash_counter, $sku_fragments));
//                                    echo "<pre>";
//                                    var_dump($new_sku);
//                                    echo "</pre>";
                                    //$new_sku = $this->getNewSkuDashByOne($dash_counter, $sku_fragments);
                                    $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                                    if ($product) {
                                        $found[] = $this->setPrice($product, $new_sku, $reprice);
                                        $found_sku = true;
                                        break;
                                    }

                                    $dash_counter--;
                                }

                                if (!$found_sku) {
//                                    echo "<pre>";
//                                    var_dump("SCENARIO 2");
//                                    echo "</pre>";
                                    $dash_counter = count($sku_fragments);
                                    while ($dash_counter !== 0) {
                                        $new_sku = trim($this->getNewSkuDashByOne($dash_counter, $sku_fragments));
//                                        echo "<pre>";
//                                        var_dump($new_sku);
//                                        echo "</pre>";
                                        $product = wc_get_product(wc_get_product_id_by_sku($new_sku));
                                        if ($product) {
                                            $found[] = $this->setPrice($product, $new_sku, $reprice);
                                            $found_sku = true;
                                            break;
                                        }

                                        $dash_counter--;
                                    }
                                }

                                if (!$found_sku) {
                                    $not_found[] = "<pre>SKU: " . $sku . " REPRICE: " . doubleval($reprice) . "</pre>";
                                }
                            }
                        }

                    }
                }
                $found[] = "<pre>============================================================</pre>";
                $not_found[] = "<pre>============================================================</pre>";

                foreach ($found as $f) {
                    echo $f;
                }

                foreach ($not_found as $nf) {
                    echo $nf;
                }
                wp_die();


            }
        }

    }
}


$skuology = new Skulogy();