<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Searchable;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\SourceQuery;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\PostsSourceQuery;

class Connector extends \PDO {

	public function __construct() {
	}

	public function getAttribute( $attribute ) {
		return false;
	}

	public function query( $query ) {

		$args = array(
			'trigrams' => true
		);

		$itemsSet = get_option( AsyncProcess::CURRENT_ITEMS_SET_OPTION_KEY );

		$postType = ! empty( $itemsSet[0] ) ? get_post_type( $itemsSet[0] ) : 'product';

		if ( ! empty( $itemsSet ) ) {
			$args['package'] = $itemsSet;
		}

		if ( $postType === 'product' ) {
			$source = new SourceQuery( $args );
		} else {
			$args['postTypes'] = array( $postType );
			$source            = new PostsSourceQuery( $args );
		}

		return new ResultObject( $source->getData() );
	}

}
