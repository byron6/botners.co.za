<?php
/**
 * @var Shipping_Quote $quote
 */
use WooCommerce_Contact_for_Shipping_Quote\Shipping_Quote;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	$cart = $quote->get_cart_contents();
	$cart_total = array_reduce( $cart, function( $total, $item ) {
		return $total += ( ($item['line_total'] + $item['line_tax']) * $item['quantity'] );
	}, 0 );

	?><tr>
		<td class="customer">
			<div class="customer-name"><?php
				echo $quote->get_customer_name();
				if ( $customer_id = $quote->get_customer_id() ) :
					echo ' <a href="' . get_edit_user_link( $customer_id ) . '"><small>' . sprintf( '(#%d)', $customer_id ) . '</small></a>';
				endif;
			?></div>

			<div class="customer-email"><?php
				if ( $email = $quote->get_customer_email() ) :
					?><a href="mailto: <?php echo esc_attr( $email ); ?>"><?php echo esc_html( $email ); ?></a><?php
				endif;
			?></div><?php

			?><div class="customer-address"><?php
				$address = WC()->countries->get_formatted_address( $quote->get_address() );
				echo esc_html( preg_replace( '/<br\s*\/?>/i', ', ', $address ) );
			?></div>
		</td>
		<td class="status"><?php
			if ( $quote->get_status_slug() == 'completed' && $order = $quote->get_order() ) :
				?><a href="<?php echo esc_url( $order->get_edit_order_url() ); ?>" class="tips" data-tip="<?php echo sprintf( __( 'Order #%s', 'woocommerce-contact-for-shipping-quote' ), $order->get_order_number() ); ?>"><?php
			endif;

			?><mark class="shipping-quote-status status-<?php echo esc_attr( $quote->get_status_slug() ); ?>">
				<span><?php echo esc_html( $quote->get_status() ); ?></span>
			</mark><?php
			if ( $quote->get_status_slug() == 'completed' && $order = $quote->get_order() ) :
				?></a><?php
			endif;

			?><div class="wccsq-request-time tips" data-tip="<?php echo $quote->get_created()->format( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ) ); ?>">
				<?php echo sprintf( _x( '%s ago', 'XX time ago', 'woocommerce-contact-for-shipping-quote' ), human_time_diff( $quote->get_created()->format( 'U' ) ) ); ?>
			</div><?php
		?></td>
		<td class="cart">
			<a href="javascript:void(0);" class="wccsq-shipping-quote-items"><?php
				echo implode( ', ', array_map( function( $item ) {
					return sprintf( '%s x %d', $item['name'], $item['quantity'] );
				}, $cart ) );
			?></a>
			<table class="wccsq-products-table widefat striped" style="display: none;">
				<tbody><?php
					foreach ( $cart as $item ) :
						$id = $item['variation_id'] ?: $item['product_id'];
						$item_total = wc_price( ($item['line_total'] + $item['line_tax']) );
						$tip = sprintf( __( '%dx %s', 'woocommerce-contact-for-shipping-quote' ), $item['quantity'], strip_tags( wc_price( ($item['line_total'] + $item['line_tax']) / $item['quantity'] ) ) );

						?><tr>
							<td class="quantity"><?php echo absint( $item['quantity'] ) . 'x '; ?></td>
							<td class="image"><?php echo get_the_post_thumbnail( $id, array( 25, 25 ) ); ?></td>
							<td class="name"><a href="<?php echo esc_url( get_permalink( $id ) ); ?>"><?php echo wp_kses_post( $item['name'] ); ?></a></td>
							<td class="total">
								<span class="tips" data-tip="<?php echo $tip; ?>"><?php echo $item_total; ?></span>
							</td>
						</tr><?php
					endforeach;
				?></tbody>
			</table>
		</td>
		<td class="quoted-amount">
			<form class="shipping-quote-actions">
				<input type="hidden" name="quote_id" value="<?php echo $quote->get_id(); ?>">
				<input type="hidden" name="quote_action" value="update_quotation_amount">

				<div class="quoted-amount-price"><?php
					if ( ! empty( $quote->get_quote_amount() ) ) :
						echo wc_price( $quote->get_quote_amount() );
					else :
						echo '&dash;';
					endif;
				?></div>
				<input type="text" class="short wc_input_price hidden quote_amount" name="quote_amount" value="<?php echo $quote->get_quote_amount(); ?>">
				<a href="javascript:void(0);" class="save-quoted-amount hidden"><?php _e( 'Save', 'woocommerce-contact-for-shipping-quote' ); ?></a>
				<a href="javascript:void(0);" class="cancel-quoted-amount hidden"><?php _e( 'Cancel', 'woocommerce-contact-for-shipping-quote' ); ?></a>
				<a href="javascript:void(0);" class="edit-quoted-amount"><?php _e( 'Edit', 'woocommerce-contact-for-shipping-quote' ); ?></a>
			</form>
		</td>
		<td class="actions">
			<form class="shipping-quote-actions">
				<input type="hidden" name="quote_id" value="<?php echo $quote->get_id(); ?>">

				<select name="quote_action">
					<option value=""><?php _e( 'Choose an action...', 'woocommerce' ); ?></option>

					<optgroup label="Status update"><?php
						foreach ( \WooCommerce_Contact_for_Shipping_Quote\get_statuses() as $slug => $status ) :
							?><option value="update_status-<?php echo esc_attr( $slug ); ?>"><?php echo sprintf( __( 'Update to %s', 'woocommerce-contact-for-shipping-quote' ), $status ); ?></option><?php
						endforeach;
						?></optgroup>

					<option value="delete"><?php _e( 'Delete', 'woocommerce-contact-for-shipping-quote' ); ?></option>
				</select>
				<button class="button wc-reload"><span><?php _e( 'Apply', 'woocommerce' ); ?></span></button>
			</form>
		</td>
	</tr>
