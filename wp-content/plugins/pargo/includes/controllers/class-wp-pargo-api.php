<?php


class Wp_Pargo_Api
{

	public function __construct()
	{


	}

	/**
	 * @param $url
	 * @param array $body
	 * @param array $headers
	 * @return string
	 */
	public function postApi($url, $body = array(), $headers = array())
	{


		$response = wp_remote_post(
			$url,
			array(
				'method'      => 'POST',
				'timeout'     => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking'    => true,
				'headers'     => $headers,
				'body'        => $body,
				'cookies'     => array()
			)
		);

		if ( is_wp_error( $response ) ) {
			$error_message = $response->get_error_message();
			return "Something went wrong: $error_message";
		} else {
			return $response;
		}

	}


	/**
	 * @return mixed
	 */
	public function AuthToken($isDev=false)
	{
		if ((isset($_SESSION['access_token'])) && (time() < $_SESSION['access_token_expiry'])) {
			return $_SESSION['access_token'];
		}

		$apiUrl = get_option('woocommerce_wp_pargo_settings')['pargo_url'];
		$lastChar = $apiUrl[strlen($apiUrl)-1];
		if ($lastChar !== '/') {
			$apiUrl = $apiUrl . '/';
		}


		/**
		 * Get the pargo api url
		 */
		$url = $apiUrl . 'auth';

		/**
		 * Get the pargo username and password
		 */
		$data = array(
			'username' =>  get_option('woocommerce_wp_pargo_settings')['pargo_username'],
			'password' => get_option('woocommerce_wp_pargo_settings')['pargo_password']
		);
		$headers = array();

		/**
		 * Get Auth data from the API
		 */
		$returnData = $this->postApi($url, $data, $headers);

		/**
		 * Check if an error is returned else return the authToken
		 */
		if ( is_wp_error( $returnData ) ) {

			$error_message = $returnData->get_error_message();
			return "Something went wrong: $error_message";

		} else {

			$response = wp_remote_retrieve_body( $returnData );

			$accessToken = json_decode($response)->access_token;
			$expiresIn = json_decode($response)->expires_in;
			$_SESSION['access_token'] = $accessToken;
			$_SESSION['access_token_expiry'] = $expiresIn;

			if($isDev){
				return $returnData;
			} else {
				return $accessToken;
			}

		}

	}

	public function getAuthToken($isDev=false)
	{
		if ((isset($_SESSION['access_token'])) && (time() < $_SESSION['access_token_expiry'])) {
			return $_SESSION['access_token'];
		}

		$apiUrl = get_option('woocommerce_wp_pargo_settings')['pargo_url'];
		$lastChar = $apiUrl[strlen($apiUrl)-1];
		if ($lastChar !== '/') {
			$apiUrl = $apiUrl . '/';
		}


		/**
		 * Get the pargo api url
		 */
		$url = $apiUrl . 'auth';

		/**
		 * Get the pargo username and password
		 */
		$data = array(
			'username' =>  get_option('woocommerce_wp_pargo_settings')['pargo_username'],
			'password' => get_option('woocommerce_wp_pargo_settings')['pargo_password']
		);
		$headers = array();

		/**
		 * Get Auth data from the API
		 */
		$returnData = $this->postApi($url, $data, $headers);

		/**
		 * Check if an error is returned else return the authToken
		 */
		if ( is_wp_error( $returnData ) ) {

			$error_message = $returnData->get_error_message();
			return "Something went wrong: $error_message";

		} else {

			$response = wp_remote_retrieve_body( $returnData );

			$accessToken = json_decode($response)->access_token;
			$expiresIn = json_decode($response)->expires_in;
			$_SESSION['access_token'] = $accessToken;
			$_SESSION['access_token_expiry'] = $expiresIn;

			if($isDev){
				return $returnData;
			} else {
				return $accessToken;
			}

		}

	}


	/**
	 * @package verifyUserCredentials - Used to verify user account on PargoAPI
	 *
	 * @param $api_url
	 * @param $username
	 * @param $password
	 *
	 * @return bool
	 */
	public function verifyUserCredentials($api_url, $username, $password)
	{

		$lastChar = $api_url[strlen($api_url)-1];
		if ($lastChar !== '/') {
			$api_url = $api_url . '/';
		}

		/**
		 * Get the pargo api url
		 */
		$url = $api_url . 'auth';

		/**
		 * Get the pargo username and password
		 */
		$data = array(
			'username' => $username,
			'password' => $password
		);
		$headers = array();

		/**
		 * Get Auth data from the API
		 */
		$returnData = $this->postApi($url, $data, $headers);

		/**
		 * Check if an error is returned else return the authToken
		 */
		if ( is_wp_error( $returnData ) ) {

			return false;

		} else {

			$response = wp_remote_retrieve_body( $returnData );

			if(isset(json_decode($response)->access_token)){
				return true;
			}

		}

		return false;

	}

}




