<?php

/**
 * Plugin Name: Pargo Shipping
 * Description: Pargo is a convenient logistics solution that lets you collect and return parcels at Pargo parcel points throughout the country when it suits you best.
 * Version: 2.4.12
 * Author: Pargo
 * Author URI: https://www.pargo.co.za
 * License: GPL-3.0+
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

// DEFINING SOME GLOBALS
define('PARGOPATH', str_replace('\\', '/', rtrim(__DIR__, '/')) . '/');
define("PARGOPLUGINVERSION", "2.4.12");
define("PARGOPLUGINDIR", plugin_dir_path(__FILE__));
define("PARGOPLUGINURL", plugin_dir_url(__FILE__));
define("PARGOWCSETTINGS", 'admin.php?page=wc-settings&tab=shipping&section=wp_pargo');


session_start();


//Prevent direct access to plugin
if (!defined('WPINC')) {
    die;
}
// Check that the file is not accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'We\'re sorry, but you can not directly access this file.' );
}


/**
* Hooks to the WooCommerce Thank You / Order Received function and posts the order to the Pargo API
*/
add_action('woocommerce_thankyou', 'placePargoOrder');
function placePargoOrder($order_id)
{
  if (isset($_SESSION['pargo_shipping_address'])) {
    echo "<script>localStorage.removeItem('pargo-shipping-settings');</script>";
    $order = new WC_Order($order_id);

    // Include the PargoOrders class for managing PargoOrders
    include PARGOPATH . 'PargoOrders.php';

    $pargoOrders = new PargoOrders();
    $pargoOrders->postOrder($order);

  }
}

add_action('woocommerce_admin_order_data_after_shipping_address', 'pargo_custom_row_after_order_addresses', 10, 1);
function pargo_custom_row_after_order_addresses($order)
{
  $meta = $order->get_meta_data();

  $check = false;

  foreach ($meta as $key => $value) {
      if (in_array('pargo_pc', $value->get_data())) {
          $check = true;
      };
  }
  echo $check;
  if ($check) {
  ?>
    </div>
    </div>
    <div class="clear"></div>
    <!-- new custom section row -->
    <div class="order_data_column_container">
      <div class="order_data_column_wide">
        <h3 style="margin-bottom: 5px"><?php _e("Pargo Shipment"); ?></h3>
        <?php
        if (isset($_GET['ship-now'])) {

            include plugin_dir_path(__FILE__) . 'PargoOrders.php';
            $pargoOrders = new PargoOrders();
            $pargoOrders->postOrder($order);

        }

        include plugin_dir_path(__FILE__) . 'PargoApi.php';
        $pargoApi = new PargoApi();
        $accessToken = $pargoApi->AuthToken();

        if (($accessToken != null || $accessToken != '') && get_option('woocommerce_wp_pargo_settings')['pargo_use_api'] == 'yes') {

          if (get_post_meta($order->get_id(), 'pargo_waybill', true) == '' && get_post_meta($order->get_id(), 'pargo_waybill', true) == null) {
            ?>
            <a style="display: inline-block;
            width: auto;
            background: #006799;
            padding: 5px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-weight: bold;
            pointer-events: none;
            text-decoration: none;" href="<?php echo $_SERVER['REQUEST_URI'] . '&ship-now' ?>" id="ship-now-pargo" class="btn btn-primary">
              Ship now
            </a>
            <br><br>

              <script>
                jQuery(document).ready(function($) {
                  jQuery('#ship-now-pargo').css('pointer-events', 'auto');
                  jQuery("#ship-now-pargo").attr("href", "<?php echo $_SERVER['REQUEST_URI'] . '&ship-now' ?>&warehouseCode=" + jQuery(this).val())
                });
              </script>
                <?php
                $payload = json_encode($order);
                $orderUrl = get_option('woocommerce_wp_pargo_settings')['pargo_url'] . 'orders';

                $data = array(
                  "data" => array(
                    "type" => "W2P",
                    "version" => 1,
                    "attributes" => array(
                      "warehouseAddressCode" => null,
                      "returnAddressCode" => null,
                      "trackingCode" => null,
                      "externalReference" => (string) $order->get_id(),
                      "pickupPointCode" => $_SESSION['pargo_shipping_address']['pargoPointCode'],

                      "consignee" => array(
                        "firstName" => $order->data['billing']['first_name'],
                        "lastName" => $order->data['billing']['last_name'],
                        "email" => $order->data['billing']['email'],
                        "phoneNumbers" => array(
                          $order->data['billing']['phone'],
                        ),
                        "address1" => $order->data['billing']['address_1'],
                        "address2" => $order->data['billing']['address_2'],
                        "suburb" => "",
                        "postalCode" => $order->data['billing']['postcode'],
                        "city" => $order->data['billing']['city'],
                        "country" => "ZA"
                      ),
                    ),
                  ),
                );

                $headers = array(
                  "Authorization: Bearer $accessToken",
                  "Content-Type: application/json",
                  "cache-control: no-cache"
                );

                array_push($headers, 'Content-Length: ' . strlen($payload));

                $response = wp_remote_post(
                  $orderUrl,
                  array(
                    'method'      => 'POST',
                    'timeout'     => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking'    => true,
                    'headers'     => $headers,
                    'body'        => $data,
                    'cookies'     => array()
                  )
                );


              } else {
            ?>

              <a target="_blank" style="display: inline-block;
      width: auto;
      background: #006799;
      padding: 5px;
      text-align: center;
      border-radius: 5px;
      color: white;
      font-weight: bold;
      text-decoration: none;" href="https://pargo.co.za/track-trace/?code=<?php echo get_post_meta($order->get_id(), 'pargo_waybill', true)['waybill']; ?>" class="btn btn-primary">
                Track and trace
              </a>
            <?php
          }
        } else {
          echo "<p>In order to ship this parcel enable the <strong>'Pargo backend service'</strong> which you can find in the admin panel of pargo plugin. Also make sure you have entered the correct username and password. </p>";
        }
      }

}

/**
* Script that runs when plugin is activated
 */
register_activation_hook(__FILE__, 'pargo_plugin_activate');
add_action('admin_init', 'pargo_plugin_redirect');
function pargo_plugin_activate()
{
    global $wpdb;

    $redirect = null;
    $table = $wpdb->prefix . 'postmeta';
    //decode data if we want the data
//    $data = $wpdb->get_row("SELECT * FROM  $table WHERE  meta_key = 'pargo_settings'");

    $plugin_location = plugin_dir_path(__FILE__) . 'pargo-shipping.php';
    $plugin_current_version = get_plugin_data($plugin_location, $markup = true, $translate = true)['Version'];


    $client_id = '3';
    $client_secret = 'L8CBIeDsHNvjJO99mUa4Ooori0cQHeKXrgtu0wYr';
    $clientData = array(
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'grant_type' => 'client_credentials'
    );
    $url = "https://monitor.pargosandbox.co.za/oauth/token";
    $headers = array();

    // Include the PargoApi class for managing API calls
    include_once plugin_dir_path(__FILE__) . 'PargoApi.php';

    $pargoApi = new PargoApi();
    $accessToken = $pargoApi->monitorAuthToken($url, $clientData, $headers);


    $data = array(
        'platform' => 'WordPress',
        'plugin_version' => $plugin_current_version,
        'client' => get_option('blogname'),
        'client_domain' => get_option('siteurl'),
        'status' => 'INSTALLED',
        'plugin_date' => date("Y-m-d H:i:s")
    );

    $headers = array(
          "Accept: application/json",
          "Authorization: Bearer $accessToken",
          "Cache-Control: no-cache",
          "Content-Type: application/x-www-form-urlencoded",
          "content-type: multipart/form-data;"
        );

    $url = "https://monitor.pargosandbox.co.za/api/plugin/submit";

    $response = $pargoApi->postApi($url, $data, $headers);

    $result = wp_remote_retrieve_body( $response );


    $client = array(
        'client' => get_option('siteurl'),
        'status' => 'ACTIVE'
    );

    $url = "https://monitor.pargosandbox.co.za/api/plugin/client";
    $response = wp_remote_post(
      $url,
      array(
        'method'      => 'POST',
        'timeout'     => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking'    => true,
        'headers'     => array(
          "Accept: application/json",
          "Authorization: Bearer $accessToken",
          "Cache-Control: no-cache",
          "Content-Type: application/x-www-form-urlencoded",
          "content-type: multipart/form-data;"
        ),
        'body'        => $client,
        'cookies'     => array()
      )
    );
    $result = wp_remote_retrieve_body( $response );


    $data = $wpdb->get_row("SELECT * FROM  $table WHERE  meta_key = 'pargo_settings'");
    if( empty($data) || count($data, 1) == 0 ) {
//        $redirect = 'admin.php?page=pargo-shipping-optin';
        $redirect = 'admin.php?page=pargo-accounts';
    } else {
        $redirect = 'admin.php?page=pargo-shipping';
    }
    add_option('redirect', $redirect);
}

function pargo_redirect() {
    if (get_option('redirect')) {
        delete_option('redirect');
         wp_redirect('redirect');
         //wp_redirect() does not exit automatically and should almost always be followed by exit.
         exit;
    }
}

function pargo_plugin_redirect()
{

    if (get_option('redirect')) {
        wp_redirect(get_option('redirect'));
        delete_option('redirect');
        exit;
    }

}




/**
  * Check if WooCommerce is active
  */
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

    /////////////////////////////////////////Admin Menu
    /**
    * Admin menu
    */
    add_action('admin_menu', 'pargo_plugin_menu');
    function pargo_plugin_menu()
    {
        add_menu_page(
            "Pargo Plugin Options",
            "Pargo",
            "manage_options",
            "pargo-shipping",
            "pargo_accounts_page",
            "dashicons-cart"
        );

        add_submenu_page(
            "pargo-shipping", //Required. Slug of top level menu item
            "Pargo - Accounts", //Required. Text to be displayed in title.
            "Pargo Account", //Required. Text to be displayed in menu.
            "manage_options", //Required. The required capability of users.
            "pargo-shipping", //Required. A unique identifier to the sub menu item.
            "pargo_accounts_page", //Optional. This callback outputs the content of the page associated with this menu item.
            null //Optional. The URL of the menu item icon
        );

        add_submenu_page(
            "pargo-shipping", //Required. Slug of top level menu item
            "Pargo - Settings", //Required. Text to be displayed in title.
            "Pargo Settings", //Required. Text to be displayed in menu.
            "manage_options", //Required. The required capability of users.
            "pargo-shipping-method", //Required. A unique identifier to the sub menu item.
            "pargo_woocom_shipping", //Optional. This callback outputs the content of the page associated with this menu item.
            null //Optional. The URL of the menu item icon
        );


        add_submenu_page(
            "pargo-shipping", //Required. Slug of top level menu item
            "Pargo - Help", //Required. Text to be displayed in title.
            "Pargo Help", //Required. Text to be displayed in menu.
            "manage_options", //Required. The required capability of users.
            "pargo-help", //Required. A unique identifier to the sub menu item.
            "pargo_shipping_help_page", //Optional. This callback outputs the content of the page associated with this menu item.
            null //Optional. The URL of the menu item icon
        );


    }
    ////////////////////////////////////////EndOF-Admin menu


    /////////////////////////////////////////Admin Pages
    /**
    * Pargo options page
     */
    function pargo_plugin_options()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        include_once 'pargo-admin.php';
    } // end pargo_plugin_options

    function pargo_woocom_shipping(){
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        $settings_link = 'admin.php?page=wc-settings&tab=shipping&section=wp_pargo';
        header("Location: $settings_link");
    }

    /**
    * Pargo Support page
     */
    function pargo_shipping_support_page()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        include_once 'includes/views/admin/pargo-support.php';
    } //end function pargo_shipping_optin_page()

    /**
    * Pargo Support page
     */
    function pargo_shipping_help_page()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        include_once 'includes/views/admin/pargo-help.php';
    } //end function pargo_shipping_optin_page()

    /**
    *   Pargo Accounts Page
    */
    function pargo_accounts_page()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        include_once 'includes/views/admin/pargo-credentials.php';

    }
    ////////////////////////////////////////////EndOF-Admin Pages


    add_filter('woocommerce_checkout_get_value', 'set_shipping_zip', 10, 2);
    function set_shipping_zip()
    {

        require_once PARGOPATH . "includes/controllers/class-wp-pargo-shipping-processes.php";
        $shippingProcesses = new Wp_Pargo_Shipping_Processes();
        $shippingProcesses->setShippingZip();

    }
    add_action('woocommerce_before_checkout_form', 'set_shipping_zip');


    function override_shipping_logic($fields)
    {
        require_once PARGOPATH . "includes/controllers/class-wp-pargo-shipping-processes.php";
        $shippingProcesses = new Wp_Pargo_Shipping_Processes();
        return $shippingProcesses->overrideShippingLogic($fields);
    }
    add_filter('woocommerce_shipping_fields', 'override_shipping_logic');


    function pargo_shipping_method()
    {

        if (!class_exists('Wp_Pargo_Shipping_Method')) {

            add_action('woocommerce_after_checkout_form', 'bbloomer_disable_shipping_local_pickup');

            function bbloomer_disable_shipping_local_pickup($available_gateways)
            {
                global $woocommerce;

                // Part 1: Hide shipping based on the static choice @ Cart
                // Note: "#customer_details .col-2" strictly depends on your theme

                $chosen_methods = WC()->session->get('chosen_shipping_methods');
                $chosen_shipping_no_ajax = $chosen_methods[0];
                if (0 === strpos($chosen_shipping_no_ajax, 'wp_pargo')) {

                ?>
                    <script type="text/javascript">
                        jQuery('#customer_details .col-2').fadeOut();
                    </script>
                <?php

                }

                // Part 2: Hide shipping based on the dynamic choice @ Checkout
                // Note: "#customer_details .col-2" strictly depends on your theme

                ?>
                <script type="text/javascript">
                    jQuery('form.checkout').on('change', 'input[name^="shipping_method"]', function() {
                        var val = jQuery(this).val();
                        if (val.match("^wp_pargo")) {
                            jQuery('#customer_details .col-2').fadeOut();
                        } else {
                            jQuery('#customer_details .col-2').fadeIn();
                        }
                    });
                </script>
            <?php

            }

            require_once 'includes/controllers/class-wp-pargo-shipping-method.php';

        }
    }
    add_action('woocommerce_shipping_init', 'pargo_shipping_method');

    function add_pargo_shipping_method($methods)
    {
        $methods['wp_pargo'] = 'Wp_Pargo_Shipping_Method';
        return $methods;
    }
    add_filter('woocommerce_shipping_methods', 'add_pargo_shipping_method');

    /**
     * Yellow Button and Display*
     */
    add_filter('woocommerce_cart_shipping_method_full_label', 'wc_pargo_label_change', 10, 2);
    function wc_pargo_label_change($label, $method)
    {
        require_once PARGOPATH . "includes/controllers/class-wp-pargo-shipping-processes.php";
        $shippingProcesses = new Wp_Pargo_Shipping_Processes();
        return $shippingProcesses->wcPargoLabelChange($label, $method);


    }

    add_action('woocommerce_after_checkout_validation', 'pargo_after_checkout_validation');
    function pargo_after_checkout_validation($posted)
    {
        if (isset($_SESSION['pargo_shipping_address'])) {
            // unset($_SESSION['shipping_address']);

            add_action('woocommerce_checkout_update_order_meta', function ($order_id, $posted) {
                $post = $_SESSION['pargo_shipping_address']['pargoPointCode'];
                $pargo_del_add = "" . $_SESSION['pargo_shipping_address']['storeName'] . ", " . $_SESSION['pargo_shipping_address']['storeName'] . ", " . $_SESSION['pargo_shipping_address']['address1'] . ", " . $_SESSION['pargo_shipping_address']['address2'] . ", " . $_SESSION['pargo_shipping_address']['city'] . ", " . $_SESSION['pargo_shipping_address']['province'] . ", " . $_SESSION['pargo_shipping_address']['postalcode'] . "";
                $order = wc_get_order($order_id);
                $order->update_meta_data('pargo_pc', $post);
                $order->update_meta_data('pargo_delivery_add', $pargo_del_add);
                $order->save();
            }, 10, 2);
        }
    }

    /**
     * Display field value on the order edit page
     */
    add_action('woocommerce_admin_order_data_after_billing_address', 'pargo_checkout_field_display_admin_order_meta', 10, 1);
    function pargo_checkout_field_display_admin_order_meta($order)
    {
        $meta = $order->get_meta_data();

        foreach ($meta as $key => $value) {
            if (in_array('pargo_pc', $value->get_data())) {
                global $woocommerce;
                echo '<p><b>' . __('Pargo Pick Up Address') . ':</b> ' . get_post_meta($order->get_id(), 'pargo_delivery_add', true) . '</p>';
                echo '<p><b>' . __('Pargo Pick Up Point Code') . ':</b> ' . get_post_meta($order->get_id(), 'pargo_pc', true) . '</p>';
            };
        }
    }

    /**
     * Pargo Modals
     */
    add_action('wp_footer', 'pargo_modals');
    function pargo_modals()
    {

        echo '<div id="pargo-not-selected" role="dialog" aria-labelledby="mySmallModalLabel">' .
            '<div id="pargo_center">' .
            '<div id="pargo_inner">' .
            '<div class="pargo_blk_title"><center><img src="' . PARGOPLUGINURL . 'assets/images/alert.png" /></center></div>' .
            '<div class="pargo_close">x</div>' .
            '<div class="pargo_content">' .
            '<p>You forgot to select your pick up point!<br />' .
            'Remember, we have pick up locations throughout the country, just pick one!' .
            '</p><img src="' . PARGOPLUGINURL . 'assets/images/click_point.png" />' .
            '</div>' .
            '</div>' .
            '</div>';
        echo '</div>';
    }

    /**
    * Validate the order
     */
    function pargo_validate_order($posted)
    {
        require_once PARGOPATH . "includes/controllers/class-wp-pargo-shipping-processes.php";
        $shippingProcesses = new Wp_Pargo_Shipping_Processes();
        $shippingProcesses->pargoValidateOrders($posted);

    } //end function Pargo weight warning
    add_action('woocommerce_review_order_before_cart_contents', 'pargo_validate_order', 10);
    add_action('woocommerce_after_checkout_validation', 'pargo_validate_order', 10);

    add_action('woocommerce_order_details_after_order_table', 'pargo_custom_field_display_cust_order_meta', 10, 1);
    function pargo_custom_field_display_cust_order_meta($order)
    {

        if (isset($_SESSION['pargo_shipping_address']['storeName'])) {
            $storeName = $_SESSION['pargo_shipping_address']['storeName'];
        }
        if (isset($_SESSION['pargo_shipping_address']['address1'])) {
            $storeAddress = $_SESSION['pargo_shipping_address']['address1'];
        }
        if (isset($_SESSION['pargo_shipping_address']['address1'])) {
            echo '<p><strong>' . __('Pargo Pickup Address') . ':</strong> ' . $storeName . '. ' . $storeAddress . '</p>';
        }

        if (isset($_SESSION['pargo_shipping_address'])) {
            //unset($_SESSION['pargo_shipping_address']);
        }
    }

    add_filter('style_loader_src',  'sdt_remove_ver_css_js', 9999, 2);
    add_filter('script_loader_src', 'sdt_remove_ver_css_js', 9999, 2);
    function sdt_remove_ver_css_js($src, $handle)
    {
        $handles_with_version = ['style']; // <-- Adjust to your needs!

        if (strpos($src, 'ver=') && !in_array($handle, $handles_with_version, true))
            $src = remove_query_arg('ver', $src);

        return $src;
    }

    add_action('wp_footer', 'woocommerce_add_gift_box');
    function woocommerce_add_gift_box()
    {
        if (is_checkout()) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function($) {
                    jQuery('#parcel_test').click(function() {
                        jQuery('body').trigger('update_checkout');
                    });
                });
            </script>
          <?php
        }
    }

    add_action('woocommerce_cart_calculate_fees', 'woo_add_cart_fee');
    function woo_add_cart_fee($cart)
    {
        require_once PARGOPATH . "includes/controllers/class-wp-pargo-shipping-processes.php";
        $shippingProcesses = new Wp_Pargo_Shipping_Processes();
        $shippingProcesses->wooAddCartFee($cart);
    }

    if (get_option('woocommerce_wp_pargo_settings')['pargo_map_display'] === 'yes') {
        add_action('woocommerce_review_order_before_payment', function () {
            include_once PARGOPATH . 'includes/views/front_end/woocommerce_add_cart_fee.php';
        });
    }


}


/**
 * /////////////////////////////////////////////////////// Handling Ajax Forms Posts
 */



/**
 * The Opt-In / Opt-Out Handler
 */
add_action( 'wp_ajax_pargo_optin', 'pargo_optin' );
add_action( 'wp_ajax_nopriv_pargo_optin', 'pargo_optin' );
function pargo_optin() {

//    echo json_encode($_POST);

    include_once PARGOPATH . "includes/controllers/class-wp-pargo-shipping-method.php";
    $pargo_shipping_method = new Wp_Pargo_Shipping_Method();
   foreach($_POST as $key=>$value){
        $pargo_shipping_method->update_option($key, $value);
    }
//    echo "Option Saved Successfully";
    exit("Option Saved Successfully");
}


/**
 * The Credentials form handler.
 */
add_action( 'wp_ajax_pargo_account_credentials', 'pargo_account_credentials' );
add_action( 'wp_ajax_nopriv_pargo_account_credentials', 'pargo_account_credentials' );
function pargo_account_credentials() {

    require_once PARGOPATH . "includes/controllers/class-wp-pargo-shipping-method.php";
    $pargo_shipping_method = new Wp_Pargo_Shipping_Method();

    foreach($_POST as $key=>$value){
        $pargo_shipping_method->update_option($key, $value);
    }

    // Don't forget to exit at the end of processing
    exit("Credentials have been Saved Successfully.");
}

/**
 * The Verify Credentials form handler.
 */
add_action( 'wp_ajax_pargo_verify_credentials', 'pargo_verify_credentials' );
add_action( 'wp_ajax_nopriv_pargo_verify_credentials', 'pargo_verify_credentials' );
function pargo_verify_credentials() {

    include_once PARGOPATH . "includes/controllers/class-wp-pargo-api.php";
    $pargoApi = new Wp_Pargo_Api();
    $result = $pargoApi->verifyUserCredentials($_POST['api_url'], $_POST['username'], $_POST['password']);
    if($result){
        exit("Account verified successfully.");
    } else {
        exit("Account could not be verified");
    }
}



/**
* Loads the main Pargo Class
 */
require plugin_dir_path( __FILE__ ) . 'includes/controllers/class-wp-pargo.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    2.4.4
 */
function run_pargo() {

	$plugin = new Wp_Pargo();
	$plugin->run();

}
run_pargo();
