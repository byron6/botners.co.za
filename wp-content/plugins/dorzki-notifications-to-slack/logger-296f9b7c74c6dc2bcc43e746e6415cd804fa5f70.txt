===========================================================================
API ERROR
---------------------------------------------------------------------------
Error Time: 04-01-2021, 16:53:07
Error Code: 503
API Response: <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Server Error | Slack</title>
	<meta name="author" content="Slack">
	<style>
		/* General */
		@font-face {
			font-family: 'Slack-Lato';
			font-style: normal;
			font-weight: 400;
			src: local('☺'), url(https://slack.global.ssl.fastly.net/9d38d/fonts/lato-1-compressed/lato-regular.woff2) format('woff2'), url(https://slack.global.ssl.fastly.net/eb80/fonts/lato-1/lato-regular.woff) format('woff');
		}
		@font-face {
			font-family: 'Slack-Lato';
			font-style: normal;
			font-weight: 700;
			src: local('☺'), url(https://slack.global.ssl.fastly.net/9d38d/fonts/lato-1-compressed/lato-bold.woff2) format('woff2'), url(https://slack.global.ssl.fastly.net/eb80/fonts/lato-1/lato-bold.woff) format('woff');
		}

		* {
			-ms-box-sizing: border-box;
			-moz-box-sizing: border-box;
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
		}
		html, body {
			width: 100%;
			overflow-x: hidden;
		}
		html.desktop {
			margin-top: 30px;
		}
		body {
			margin: 0;
			font-family: 'Slack-Lato', sans-serif;
			font-variant-ligatures: common-ligatures;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
			color: #555459;
			font-size: 1.125rem;
			line-height: 1.5rem;
			background: #f9f9f9;
		}

		a, a:link, a:visited {
			color: #3aa3e3;
			text-decoration: none;
		}

		h1 {
			font-size: 2rem;
			line-height: 2.5rem;
			letter-spacing: -1px;
			font-weight: 700;
			font-family: 'Slack-Lato', sans-serif;
			margin: 0 0 1rem 0;
		}
		@media only screen and (max-width: 640px) {
			h1 {
				font-size: 1.75rem;
				line-height: 2rem;
			}
		}

		p {
			margin: 0 0 1rem 0;
		}

		/* Page */
		#page {
			width: 100%;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
		}
		#page_contents {
			margin: 0 auto;
			padding: 8rem 2rem;
		}
		@media only screen and (max-height: 768px), screen and (max-width: 640px) {
			#page_contents {
				padding: 6rem 2rem;
			}
		}
		@media only screen and (min-width: 1024px) {
			#page_contents {
				width: 1024px;
			}
		}
		@media only screen and (max-width: 640px) {
			#page_contents {
				width: 100%;
				padding: 1rem 0.8rem;
				margin-top: 70px;
			}
		}

		/* Top nav */
		nav.top.persistent, nav.top.with_color {
			background: #fff;
			box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
		}
		nav.top {
			position: fixed;
			top: 0;
			width: 100%;
			height: 70px;
			z-index: 99;
		}
		html.desktop nav.top {
			padding-top: 30px;
			height: 100px;
		}
		nav.top.persistent .logo, nav.top.with_color .logo {
			background-position: left bottom;
		}
		nav .logo {
			background-size: 120px 31px;
			height: 31px;
			width: 120px;
			float: left;
			margin: 20px 0 0 20px;
			background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAAfCAYAAAAslQkwAAAHLUlEQVR42u2afUxTVxiHKwIqouLUOY2zHXNb4rS3iIgofk2NMXM6s6ARysIWHcu+iCyiUakFEjVkm9M4N0w29s90c8t0LbZ0YsziqKBExYGKOKXUDzYpUlCnE7j7XXI68XpOObfUjSZ9kyeB+55e7+Xp+XjPUcUT8WXuqfH25v2gYkqpe0eC3f1E17xgNSZqrdk/gQqtxfCp7ogxStU7YjlYJWMM6O0xAKyiEA78G9PKWxLiS91tU0qbxQe4qxbWiv2kfIzNOBNS27WWbNGDYM0+HVtREKb6/+N3IMpYGACCnwIihSHAvwG5BySpcqYea14m5SHXIkmVo7MZXw0KDgTBGJZpguPtboOUR2+9RBOM6+uCggNAsDTnMgTnkB7soPZgi2FDUHAACJYWVNKcGxQc8ILZIS2opDkXUg0gp5Oy1jlBwQEpmD+CggNEcIK9ZRp6ZREWVJcw/DpYoE012hTMONk6QongGJthoWAx/Ky1GioF66btsYe2+vKg48EWsB+cI/JOgEKwEkT5QfAcsJvctw4cB7tAAsnHgXwZ7wJvMQHkgiPgAqgGJpAH1H4QrAX5FIwgXDX1mDveU+fyAtHnZl8W+/MIFmzGRY+usA3Hk8R9fTnFRoEC0AZEL/xFXizCB8GjgbWb+xeCdMr1Ui+SvgUdXu7ZBvaAobyCKe3qKe3awVJPGfQ9h1RaHbycS7DF8Cu9TjbM55AbAoqBqIDFCgVHg8uc93ZyCtaBKwqe+SKYoFBwf1DOaPfhgzLI7i71TXBLJqmDq2gCsW35ARF8gyrYakzjELye8QI3wW3K9WsgVIHgfuAUEHnhEDwGXPfhPk4wTIHgvYw2n8s3MvJ9E+yOVyF0xYZdFLkdgiU3Rspj3nXJ84Ilu22yNe8FDsHyHnMWTOySV4O3QQ3Jb1Y4B2cBkUE7+bxbkWDvI84N4PQybH/JKdjAyFtBX9lBgjgYkiuUzcEtW/4dAUo2D0Mv/e1huYZckqYINrTrirOzOOQOpLzAD16G8mVArUBwKGig5O+ADBAJPPESqOQQPJPR5iB4TjYt7OuSbwXZIIJDcBLjC1IJBqloMVsUQzHkLobodZhbN7DA4ipjennzZPnnk6qN4Vg4LRWKs9/z9FxPYAjPlOZjCYh/P/ZQ7kQVX/QBbobk+SC8h2VSIiXXAeZ6OeEp70ZwASUPkcz4inxmJOciax64w5iangYBFyVAZNACDoA3wCAfBK+l5PYCb/FmN4IvynL3wXA/1sGNlGu3wCTADkf064mOsSk24KxTp1yXcGj0Rk8e1+c61CkluH4ePxf+qUnDAzwIV5GwoNEkHAZXG83a62yEGpdZu7vFHMv70lPAPZZk2UvmgH4KBG+j5NJ7KPiuLHfabxsdbLyf2tVHp8Q5xia31Y1NFh9CnfyxlHdqUmch39E1h98vXIt9K6JT7kHd/EaTVlQCJJ8Rq5N4D69fUVBy2EE4p+CdlFxyDwSHUGr1o/+B4O2AHeiVe4g4qmDINNHy9Rr9ik7BZsFGxCmiyayFOO6IBCuBTVYe0djKKTibktvWwx58VZZrBaF+FFzGuJ7hTfARmkDPEA3RNbQ8xG/qFGzSVvoi2GXWrVL5FmFk6M4C1ZSXPcspeBEQKUI0gBWfdSP4R0p+I2BFGkhSIDgaXGKUdEvo868mJYcm0KnRzybzby1VMD4n5TGv7lQs16Rtd1lixnPIfBLEdbPSrqSshMM5BEcyVqTVQCf7dyLARxxlUgol/zdYDUJkz51Ocp6pZRrnTtZEcIuSu039W0lzKXrjUdrwzCO4uShxKBZXJ5XIbSoS1qr4wghEYAHTQZgsPwLUyV7UrWCjYwcQKdwHdlBAyrJmzo2OUHCe0e4y2AsKQS2jTT7nRsdrjFq4gToCiUZjSL0mdQGG5fQ6tZ4suTkEkxArYsOainQvYz7ObDLr1jAxCemk5/JEOGig/OFrifASxnz8jQLBI8Efft6qnOXpmQq5CyYp2KrMY7SpBlGAHdyCH2/ogaiQa2CUwsOGeZTyhsU9zsOGNI7TL/kwnqrwsKEPMDHaHQZhfhGMkucsht7WTlDrukzCdy5bnD92VYaDLxT8oa6ABB/Pg+fIRgsaNWA9p2DPF8fJ8dxXwXQfjwsHg3OMtl/7S7BDPs9C9EXRHBuh8k+MBmtACWiinAGfAWvBACAPK6iSkQhoMQTkUb4UF8BGcv8l4KSMwm7+A3sGsIN22YKojJRd/Rlf7ioKkZS2z4NTjPbJ/hdMcBXpklSPJ6KABowlw5Tfg8geR3qIvyKMPPMo8ty9I7DCPkHd6FDrM73VwU0m3TuqYPT+gOCtlE2OjiuaZB3Zh/7k0VJIaHMVCy+qgtH7o0GbOhDD9C9d5Lbh9yxP3mWJH4zyqNQjFz/fh+DVqmAESpA6+Rn9DKc6Jdn5rH7cI3nRGHLTFDOr6WDMiuaiuGhVMHpN/AN0GQvJQUqdZQAAAABJRU5ErkJggg==) left top no-repeat;
		}

		/* Card */
		.card {
			background-color: #fff;
			border-radius: 0.25rem;
			box-shadow: 0 1px 0 rgba(0, 0, 0, 0.25);
			padding: 2rem 2rem 1rem;
			margin: 0 auto 2rem;
			position: relative;
			border: 1px solid #e8e8e8;
		}
		@media only screen and (max-width: 640px) {
			.card {
				padding: 1rem;
			}
		}
		.warning_icon {
			margin: 0 4px;
			transform: translateY(2px);
		}
	</style>
</head>
<body>
	<nav class="top persistent">
		<a href="https://status.slack.com/" class="logo" data-qa="logo"></a>
	</nav>
	<div id="page">
		<div id="page_contents">
			<h1>
				<svg width="30px" height="27px" viewBox="0 0 60 54" class="warning_icon"><path d="M51.6 54C56.9 54 60 51 60 47 60 45.4 59.6 43.8 58.6 42.1L36.9 4.8C35 1.6 32.5 0 30 0 27.5 0 25 1.6 23.1 4.8L1.4 42.1C0.4 43.8 0 45.4 0 47 0 51 3.1 54 8.4 54L51.6 54ZM51.6 49.5L8.4 49.5C6 49.5 4.7 48.3 4.7 46.6 4.7 45.9 4.9 45.1 5.3 44.2L27.1 7C27.9 5.5 28.9 4.8 30 4.8 31.1 4.8 32.1 5.5 32.9 7L54.7 44.2C55.2 45.1 55.4 45.9 55.4 46.6 55.4 48.3 54 49.5 51.6 49.5ZM30 33C31.7 33 33 31.6 33 30L33 18C33 16.3 31.7 15 30 15 28.4 15 27 16.3 27 18L27 30C27 31.6 28.4 33 30 33ZM30 45C32.1 45 33.8 43.3 33.8 41.2 33.8 39.2 32.1 37.5 30 37.5 27.9 37.5 26.3 39.2 26.3 41.2 26.3 43.3 27.9 45 30 45Z" fill="#D94827"/></svg>
				Server Error
			</h1>
			<div class="card">
				<p>It seems like there’s a problem connecting to our servers, and we’re investigating the issue.</p>
				<p>Please <a href="https://status.slack.com/">check our Status page for updates</a>.</p>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		if (window.desktop) {
			document.documentElement.className = 'desktop';
		}

		var FIVE_MINS = 5 * 60 * 1000;
		var TEN_MINS = 10 * 60 * 1000;

		function randomBetween(min, max) {
			return Math.floor(Math.random() * (max - (min + 1))) + min;
		}

		window.setTimeout(function () {
			window.location.reload(true);
		}, randomBetween(FIVE_MINS, TEN_MINS));
	</script>
</body>
</html>

API Request: {"username":"Paul Bot-ner","icon_url":"https:\/\/bothners.co.za\/wp-content\/uploads\/2019\/02\/Screenshot-2019-02-05-23.50.15.png","text":":shopping_bags: There is a new order on *<https:\/\/bothners.co.za|Paul Bothner Music | Musical instrument stores>*!","mrkdwn":true,"attachments":[{"fallback":":shopping_bags: There is a new order on *<https:\/\/bothners.co.za|Paul Bothner Music | Musical instrument stores>*!","color":"#34495e","fields":[{"title":"Order ID","value":"509024","short":true},{"title":"Order Status","value":"Failed","short":true},{"title":"Order Total","value":"R80","short":true},{"title":"Paid Via","value":"PayGate Payment Gateway","short":true}]},{"fallback":":shopping_bags: There is a new order on *<https:\/\/bothners.co.za|Paul Bothner Music | Musical instrument stores>*!","color":"#34495e","fields":[{"title":"Product Name","value":"Nomad NGH-304R WALL HANGER *x1*","short":true},{"title":"Product Total","value":"R80","short":true}]}],"channel":"david"}
===========================================================================

