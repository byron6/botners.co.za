# Copyright (C) 2016 
# This file is distributed under the same license as the  package.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/style\n"
"POT-Creation-Date: 2017-07-29 19:42+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-04-10 14:38+0200\n"
"Language-Team: OceanWP\n"
"X-Generator: Poedit 2.0.6\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n"
"%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: _esc_attr__;esc_attr_x;esc_attr_e;esc_html__;"
"esc_html_e;esc_html_x;__;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c;_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Textdomain-Support: yes\n"
"Last-Translator: \n"
"Language: pl_PL\n"
"X-Poedit-SearchPath-0: .\n"

#: ocean-footer-callout.php:146 ocean-footer-callout.php:155
msgid "Cheatin&#8217; huh?"
msgstr "Oszukujemy&#8217; co?"

#: ocean-footer-callout.php:223
msgid ""
"Ocean Footer Callout requires that you use OceanWP as your parent theme."
msgstr ""
"Ocean Footer Callout wymaga żebyś używał OceanWP jako głównego motywu."

#: ocean-footer-callout.php:223
msgid "Install OceanWP now"
msgstr "Zainstaluj OceanWP"

#: ocean-footer-callout.php:237
msgid "Footer Callout"
msgstr "Wywołanie stopki motywu"

#: ocean-footer-callout.php:250
msgid "Page ID"
msgstr "ID strony"

#: ocean-footer-callout.php:251
msgid ""
"Deprecated, this field is no longer supported. Please use the Select "
"Template field below instead."
msgstr ""
"Pole nie jest już dłużej wspierane. Użyj pola \"Wybierz szablon\" "
"poniżej."

#: ocean-footer-callout.php:266
msgid "Select Template"
msgstr "Wybierz szablon"

#: ocean-footer-callout.php:267
msgid "Choose a template created in Theme Panel > My Library."
msgstr "Wybierz stworzony szablon w Panel motywów > Moja Biblioteka"

#: ocean-footer-callout.php:285
msgid "Content"
msgstr "Zawartość"

#: ocean-footer-callout.php:300
msgid "Button"
msgstr "Przycisk"

#: ocean-footer-callout.php:314
msgid "Enable Button"
msgstr "Uaktywnij przycisk"

#: ocean-footer-callout.php:330
msgid "Button URL"
msgstr "Adres URL przycisku"

#: ocean-footer-callout.php:341
msgid "Get In Touch"
msgstr "Skontaktuj się z nami"

#: ocean-footer-callout.php:347
msgid "Button Text"
msgstr "Tekst przycisku"

#: ocean-footer-callout.php:364
msgid "Button Target"
msgstr "Cel przycisku"

#: ocean-footer-callout.php:369
msgid "Blank"
msgstr "Pusty"

#: ocean-footer-callout.php:370
msgid "Self"
msgstr "Jednolity"

#: ocean-footer-callout.php:385
msgid "Button Rel"
msgstr "Powiązanie przycisku"

#: ocean-footer-callout.php:390
msgid "None"
msgstr "Brak"

#: ocean-footer-callout.php:391
msgid "Nofollow"
msgstr "Nofollow"

#: ocean-footer-callout.php:404
msgid "Styling"
msgstr "Styl"

#: ocean-footer-callout.php:442
msgid "Padding (px)"
msgstr "Odstęp (px)"

#: ocean-footer-callout.php:470
msgid "Background"
msgstr "Tło"

#: ocean-footer-callout.php:486
msgid "Border Color"
msgstr "Kolor obramowania"

#: ocean-footer-callout.php:502
msgid "Text Color"
msgstr "Kolor tekstu"

#: ocean-footer-callout.php:518
msgid "Links"
msgstr "Linki"

#: ocean-footer-callout.php:534
msgid "Links: Hover"
msgstr "Linki po najechaniu"

#: ocean-footer-callout.php:550
msgid "Button Border Radius (px)"
msgstr "Szerokość obramowania przycisku (px)"

#: ocean-footer-callout.php:566
msgid "Button Background"
msgstr "Tło przycisku"

#: ocean-footer-callout.php:582
msgid "Button Color"
msgstr "Kolor przycisku"

#: ocean-footer-callout.php:598
msgid "Button: Hover Background"
msgstr "Kolor tła przycisku po najechaniu"

#: ocean-footer-callout.php:614
msgid "Button: Hover Color"
msgstr "Kolor przycisku po najechaniu"

#: ocean-footer-callout.php:654
msgid "Callout"
msgstr "Wywołanie"

#: ocean-footer-callout.php:664
msgid "Display Callout"
msgstr "Wyświetl wywołanie"

#: ocean-footer-callout.php:665
msgid "Enable or disable the footer callout."
msgstr "Włącz lub wyłącz wywołanie stopki motywu."

#: ocean-footer-callout.php:667
msgid "Default"
msgstr "Domyślne"

#: ocean-footer-callout.php:668
msgid "Enable"
msgstr "Uaktywnij"

#: ocean-footer-callout.php:669
msgid "Disable"
msgstr "Wyłącz"

#: ocean-footer-callout.php:686
msgid "Callout Button Url"
msgstr "Adres URL przycisku wywołania"

#: ocean-footer-callout.php:687
msgid "Enter a valid link."
msgstr "Wprowadź poprawny link."

#: ocean-footer-callout.php:703
msgid "Callout Button Text"
msgstr "Tekst przycisku wywołania"

#: ocean-footer-callout.php:704
msgid "Enter your text."
msgstr "Wpisz tekst."

#: ocean-footer-callout.php:720
msgid "Callout Text"
msgstr "Tekst wywołania"

#: ocean-footer-callout.php:721
msgid "Override the default callout text."
msgstr "Nadpisz domyślny tekst wywołania."
